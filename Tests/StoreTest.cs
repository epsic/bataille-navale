﻿using Bataillenavale.Controller;
using Bataillenavale.Model;
using Bataillenavale.StoreManager.Getters;
using Bataillenavale.StoreManager.Store;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class StoreTest
    {
        [TestMethod]
        public void TestLocalGameCreation()
        {
            StartGameController.CreateLocalGame();

            Assert.AreEqual(Data.Game.Count, 1);
            Assert.AreEqual(Data.Game[0].Id, "LocalGame");
            Assert.AreEqual(Data.Boats.Count, 10);
            Assert.AreEqual(Data.Boxes.Count, 200);
            Assert.AreEqual(Data.Grids.Count, 2);
            Assert.AreEqual(Data.Players.Count, 2);

            Assert.AreEqual(Boats.GetAllBoatsBySize(BoatSize.Medium).Count, 4);
        }
    }
}