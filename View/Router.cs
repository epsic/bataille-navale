﻿using System.Windows.Forms;

namespace Bataillenavale.View.Router
{
    public static class ViewRenderer
    {
        public static void Render(UserControl view)
        {
            Main.Instance.MainPanel.Controls.Clear();
            Main.Instance.MainPanel.Controls.Add(view, 0, 0);
            Home.Instance.Dock = DockStyle.Fill;
            Home.Instance.BringToFront();
        }
    }
}