﻿using System;
using System.IO;
using System.Media;
using System.Threading;
using System.Windows.Forms;
using Bataillenavale.Controller;

namespace Bataillenavale.View
{
    public partial class Home : UserControl
    {
        private static Home _instance;

        public Home()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;

            string fileName = "Target.cur";
            string path = Path.Combine(Environment.CurrentDirectory, @"Assets\", fileName);
            Cursor = new Cursor(path);
        }

        public static Home Instance => _instance ?? (_instance = new Home());

        private void StartGameBtn_Click(object sender, EventArgs e)
        {
            StartGameController.StartLocalGame();
            soundexplosion();
        }

        private void CreateOnlineGame_Click(object sender, EventArgs e)
        {
            soundexplosion();
        }

        private void JoinOnlineGame_Click(object sender, EventArgs e)
        {
            soundexplosion();
        }

        private void Home_MouseClick(object sender, MouseEventArgs e)
        {
            soundbtnexplosion();
        }

        public void soundbtnexplosion()
        {
            SoundPlayer player = new SoundPlayer();
            player = new System.Media.SoundPlayer(@"Assets\sound\explosion_01.wav");
            player.Play();
        }

        public void soundexplosion()
        {
            SoundPlayer player = new SoundPlayer();
            player = new System.Media.SoundPlayer(@"Assets\sound\explosionlong.wav");
            player.Play();
        }
    }
}