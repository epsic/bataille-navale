﻿using System;
using System.Windows.Forms;
using Bataillenavale.Controller;
using Bataillenavale.StoreManager.Getters;

namespace Bataillenavale.View.PositionBoats
{
    public partial class PositionBoats : UserControl
    {
        private static PositionBoats _instance;
        public PositionBoats()
        {
            InitializeComponent();
            Dock = DockStyle.Fill;
            label1.DataBindings.Add("Text",
                Labels.GetLabelByName("label1"),
                "Text", 
                true,
                DataSourceUpdateMode.OnPropertyChanged);
        }

        public static PositionBoats Instance => _instance ?? (_instance = new PositionBoats());

        private void GoBackBtn_Click(object sender, EventArgs e)
        {
            MainController.RenderHome();
        }

        private void changeStatusBtn_Click(object sender, EventArgs e)
        {
            Labels.GetLabelByName("label1").Text = "Done";
        }
    }
}