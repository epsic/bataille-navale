﻿
namespace Bataillenavale.View
{
    partial class Home
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.StartGameBtn = new System.Windows.Forms.Button();
            this.Title = new System.Windows.Forms.Label();
            this.CreateOnlineGame = new System.Windows.Forms.Button();
            this.JoinOnlineGame = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StartGameBtn
            // 
            this.StartGameBtn.BackColor = System.Drawing.Color.Firebrick;
            this.StartGameBtn.Font = new System.Drawing.Font("Impact", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartGameBtn.ForeColor = System.Drawing.Color.Transparent;
            this.StartGameBtn.Location = new System.Drawing.Point(230, 276);
            this.StartGameBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.StartGameBtn.Name = "StartGameBtn";
            this.StartGameBtn.Size = new System.Drawing.Size(116, 71);
            this.StartGameBtn.TabIndex = 0;
            this.StartGameBtn.Text = "Start Game";
            this.StartGameBtn.UseVisualStyleBackColor = false;
            this.StartGameBtn.Click += new System.EventHandler(this.StartGameBtn_Click);
            // 
            // Title
            // 
            this.Title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Title.Font = new System.Drawing.Font("Impact", 48F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.GhostWhite;
            this.Title.Location = new System.Drawing.Point(0, 8);
            this.Title.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(1272, 101);
            this.Title.TabIndex = 1;
            this.Title.Text = "Bataille Navale";
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CreateOnlineGame
            // 
            this.CreateOnlineGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateOnlineGame.BackColor = System.Drawing.Color.Firebrick;
            this.CreateOnlineGame.ForeColor = System.Drawing.Color.Transparent;
            this.CreateOnlineGame.Location = new System.Drawing.Point(937, 276);
            this.CreateOnlineGame.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CreateOnlineGame.Name = "CreateOnlineGame";
            this.CreateOnlineGame.Size = new System.Drawing.Size(116, 71);
            this.CreateOnlineGame.TabIndex = 2;
            this.CreateOnlineGame.Text = "Create";
            this.CreateOnlineGame.UseVisualStyleBackColor = false;
            this.CreateOnlineGame.Click += new System.EventHandler(this.CreateOnlineGame_Click);
            // 
            // JoinOnlineGame
            // 
            this.JoinOnlineGame.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.JoinOnlineGame.BackColor = System.Drawing.Color.Firebrick;
            this.JoinOnlineGame.ForeColor = System.Drawing.Color.Transparent;
            this.JoinOnlineGame.Location = new System.Drawing.Point(937, 357);
            this.JoinOnlineGame.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.JoinOnlineGame.Name = "JoinOnlineGame";
            this.JoinOnlineGame.Size = new System.Drawing.Size(116, 71);
            this.JoinOnlineGame.TabIndex = 3;
            this.JoinOnlineGame.Text = "Join";
            this.JoinOnlineGame.UseVisualStyleBackColor = false;
            this.JoinOnlineGame.Click += new System.EventHandler(this.JoinOnlineGame_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Impact", 28.2F, System.Drawing.FontStyle.Italic);
            this.label1.ForeColor = System.Drawing.Color.GhostWhite;
            this.label1.Location = new System.Drawing.Point(703, 178);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(562, 84);
            this.label1.TabIndex = 4;
            this.label1.Text = "Online Game";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Impact", 28.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.GhostWhite;
            this.label2.Location = new System.Drawing.Point(7, 178);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(569, 84);
            this.label2.TabIndex = 5;
            this.label2.Text = "Local Game";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.JoinOnlineGame);
            this.Controls.Add(this.CreateOnlineGame);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.StartGameBtn);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Impact", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Home";
            this.Size = new System.Drawing.Size(1272, 1273);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Home_MouseClick);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartGameBtn;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button CreateOnlineGame;
        private System.Windows.Forms.Button JoinOnlineGame;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
