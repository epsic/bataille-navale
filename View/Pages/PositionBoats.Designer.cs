﻿namespace Bataillenavale.View.PositionBoats
{
    partial class PositionBoats
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.GoBackBtn = new System.Windows.Forms.Button();
            this.changeStatusBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Title.Location = new System.Drawing.Point(7, 6);
            this.Title.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(952, 48);
            this.Title.TabIndex = 2;
            this.Title.Text = "Position Boats";
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GoBackBtn
            // 
            this.GoBackBtn.Location = new System.Drawing.Point(11, 6);
            this.GoBackBtn.Margin = new System.Windows.Forms.Padding(4);
            this.GoBackBtn.Name = "GoBackBtn";
            this.GoBackBtn.Size = new System.Drawing.Size(100, 48);
            this.GoBackBtn.TabIndex = 3;
            this.GoBackBtn.Text = "Home";
            this.GoBackBtn.UseVisualStyleBackColor = true;
            this.GoBackBtn.Click += new System.EventHandler(this.GoBackBtn_Click);
            // 
            // changeStatusBtn
            // 
            this.changeStatusBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.changeStatusBtn.Location = new System.Drawing.Point(789, 64);
            this.changeStatusBtn.Margin = new System.Windows.Forms.Padding(4);
            this.changeStatusBtn.Name = "changeStatusBtn";
            this.changeStatusBtn.Size = new System.Drawing.Size(105, 54);
            this.changeStatusBtn.TabIndex = 5;
            this.changeStatusBtn.Text = "Change Status";
            this.changeStatusBtn.UseVisualStyleBackColor = true;
            this.changeStatusBtn.Click += new System.EventHandler(this.changeStatusBtn_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(786, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "label1";
            // 
            // PositionBoats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.changeStatusBtn);
            this.Controls.Add(this.GoBackBtn);
            this.Controls.Add(this.Title);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PositionBoats";
            this.Size = new System.Drawing.Size(965, 662);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label Title;
        private System.Windows.Forms.Button GoBackBtn;
        private System.Windows.Forms.Button changeStatusBtn;
        private System.Windows.Forms.Label label1;
    }
}
