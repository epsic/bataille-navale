﻿using System.Windows.Forms;

namespace Bataillenavale.View
{
    public partial class Main : Form
    {
        private static Main _instance;

        public Main()
        {
            InitializeComponent();
        }
        public static Main Instance => _instance ?? (_instance = new Main());
    }
}