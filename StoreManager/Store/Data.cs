﻿using System.Collections.Generic;
using Bataillenavale.Model;

namespace Bataillenavale.StoreManager.Store
{
    public static class Data
    {
        public static List<GameModel> Game { get; set; } = new List<GameModel>();
        public static List<GridModel> Grids { get; set; } = new List<GridModel>();
        public static List<PlayerModel> Players { get; set; } = new List<PlayerModel>();
        public static List<BoxModel> Boxes { get; set; } = new List<BoxModel>();
        public static List<BoatModel> Boats { get; set; } = new List<BoatModel>();
    }
}