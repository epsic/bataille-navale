﻿using System.Collections.Generic;
using Bataillenavale.Model;

namespace Bataillenavale.StoreManager.Store
{
    public static class Components
    {
        public static List<LabelReactive> Labels { get; set; } = new List<LabelReactive>
        {
            new LabelReactive("label1", "Avant Update")
        };
    }
}