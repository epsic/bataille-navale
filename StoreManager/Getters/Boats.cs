﻿using System.Collections.Generic;
using Bataillenavale.Model;
using Bataillenavale.StoreManager.Store;

namespace Bataillenavale.StoreManager.Getters
{
    public static class Boats
    {
        public static List<BoatModel> GetAllBoatsBySize(BoatSize boatSize)
        {
            return Data.Boats.FindAll(boat =>
                boat.Size == boatSize
            );
        }

        public static List<BoatModel> GetBoatsByPlayerId(int playerId)
        {
            return Data.Boats.FindAll(boat =>
                boat.PlayerId == playerId
            );
        }
    }
}