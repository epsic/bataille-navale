﻿using Bataillenavale.Model;

namespace Bataillenavale.StoreManager.Getters
{
    public static class Labels
    {
        public static LabelReactive GetLabelByName(string name)
        {
            return Store.Components.Labels.Find(label =>
                label.Name == name
            );
        }
    }
}