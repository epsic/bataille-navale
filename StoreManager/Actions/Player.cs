﻿using Bataillenavale.Model;
using Bataillenavale.StoreManager.Mutations;

namespace Bataillenavale.StoreManager.Actions
{
    public class Player
    {
        public static void CreatePlayer(string gameId, PlayerType playerType)
        {
            var playerId = Mutations.Player.CreatePlayer(gameId, playerType);
            Boat.CreatePlayerBoats(playerId);
            var gridId = Grid.CreateGrid(playerId);
            Box.CreateBoxes(gridId);
        }
    }
}