﻿using Bataillenavale.Model;
using Bataillenavale.StoreManager.Store;

namespace Bataillenavale.StoreManager.Mutations
{
    public class Game
    {
        public static void CreateGame(string id)
        {
            Data.Game.Add(new GameModel(id));
        }
    }
}