﻿using System;
using Bataillenavale.Model;
using Bataillenavale.StoreManager.Store;

namespace Bataillenavale.StoreManager.Mutations
{
    public static class Player
    {
        public static int CreatePlayer(string gameId, PlayerType playerType)
        {
            var id = Data.Players.Count + 1;
            Data.Players.Add(new PlayerModel(id, gameId, playerType));
            return id;
        }
    }
}