﻿using Bataillenavale.Model;
using Bataillenavale.StoreManager.Store;

namespace Bataillenavale.StoreManager.Mutations
{
    public static class Box
    {
        public static void CreateBoxes(int gridId)
        {
            for (var x = 1; x < 11; x++)
            for (var y = 1; y < 11; y++)
            {
                var id = Data.Boxes.Count + 1;
                Data.Boxes.Add(new BoxModel(id, gridId, BoxStatus.Normal, x, y));
            }
        }
    }
}