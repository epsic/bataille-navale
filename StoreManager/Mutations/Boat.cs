﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bataillenavale.Model;
using Bataillenavale.StoreManager.Store;

namespace Bataillenavale.StoreManager.Mutations
{
    public static class Boat
    {
        public static void CreatePlayerBoats(int playerId)
        {
            int id;
            foreach (var boatSize in (BoatSize[])Enum.GetValues(typeof(BoatSize)))
            {
                id = Data.Boats.Count + 1;
                Data.Boats.Add(new BoatModel(id, playerId, boatSize));
            }

            id = Data.Boats.Count + 1;
            Data.Boats.Add(new BoatModel(id, playerId, BoatSize.Medium));
        }
    }
}
