﻿using Bataillenavale.Model;
using Bataillenavale.StoreManager.Store;

namespace Bataillenavale.StoreManager.Mutations
{
    public static class Grid
    {
        public static int CreateGrid(int playerId)
        {
            var id = Data.Grids.Count + 1;
            Data.Grids.Add(new GridModel(id, playerId));
            return id;
        }

 
    }
}