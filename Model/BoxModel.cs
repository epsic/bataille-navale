﻿using System.Drawing;

namespace Bataillenavale.Model
{
    public enum BoxStatus
    {
        Hit,
        Destroy,
        Boat,
        Normal,
        Miss
    }

    public class BoxModel
    {
        public BoxModel(int id, int gridId, BoxStatus boxStatus, int x, int y, int? boatId = null)
        {
            Id = id;
            GridId = gridId;
            BoxStatus = boxStatus;
            Coordinates = new Point(x, y);
            BoatId = boatId;
        }

        public int Id { get; set; }
        public int? BoatId { get; set; }
        public int GridId { get; set; }
        public Point Coordinates { get; set; }
        public BoxStatus BoxStatus { get; set; }
    }
}