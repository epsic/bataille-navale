﻿namespace Bataillenavale.Model
{
    public class GridModel
    {
        public GridModel(int id, int playerId)
        {
            Id = id;
            PlayerId = playerId;
        }

        public int Id { get; set; }

        public int PlayerId { get; set; }
    }
}