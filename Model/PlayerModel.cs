﻿namespace Bataillenavale.Model
{
    public enum PlayerType
    {
        Human,
        Computer
    }

    public class PlayerModel
    {
        public PlayerModel(int id, string gameId, PlayerType playerType, string pseudo = "")
        {
            Id = id;
            GameId = gameId;
            PlayerType = playerType;
            Pseudo = pseudo;
        }

        public int Id { get; set; }
        public PlayerType PlayerType { get; set; }

        public string GameId { get; set; }
        public string Pseudo { get; set; }
    }
}