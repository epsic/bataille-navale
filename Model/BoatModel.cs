﻿namespace Bataillenavale.Model
{
    public enum AlignmentType
    {
        Horizontal,
        Vertical,
        None
    }

    public enum BoatSize
    {
        Huge = 5,
        Big = 4,
        Medium = 3,
        Small = 2
    }

    public class BoatModel
    {
        public BoatModel(int id, int playerId, BoatSize size, AlignmentType alignment = AlignmentType.None)
        {
            Id = id;
            PlayerId = playerId;
            Size = size;
            Alignment = alignment;
        }

        public int Id { get; set; }


        public int PlayerId { get; set; }

        public BoatSize Size { get; set; }

        public AlignmentType Alignment { get; set; }
    }
}