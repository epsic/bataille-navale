# Bataille Navale

## EXERCICE – BATAILLE NAVALE
### ENONCÉ
Par groupe de 3 personnes, réalisez un petit projet reflétant le jeu de la bataille navale.
Ce jeu se jouera contre un ordinateur (IA).

Vous trouverez les règles du jeu sur ce lien 
[wikipedia](https://fr.wikipedia.org/wiki/Bataille_navale_(jeu))

### La grille 

Doit faire une taille de 10x10 cases et il doit y avoir 5 bateaux disposés de
cette façon:
 - 1 x 5 cases
 - 1 x 4 cases
 - 2 x 3 cases
 - 1 x 2 cases

### Documentation

 - [ ] Vous devez en parallèle faire une documentation technique représentant 
 les choix entrepris pour réaliser ce projet.

 - [ ] La documentation devra contenir les différents modèles de données (MCD/MLD).
 - [ ] Explication sur la structure de l’application.
 - [ ] Explication sur la stratégie appliquéepour la gestion de l’IA et des 
 explications sur les différents cas de tests.


## TÂCHES
- [ ] Réalisation du MCD/MLD
- [ ] Poser l’architecture du projet
- [ ] Documenter la solution choisie
- [ ] Réalisation du projet
- [ ] Définir des cas de tests

## CONTRAINTES

 - La documentation doit impérativement être faîte selon les consignes données.

 - L’IA doit avoir un minimum d’intelligence et ne pas se contenter de tirer des
bombes de manière aléatoire.