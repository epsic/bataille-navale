﻿namespace Bataillenavale.Model
{
    public class LabelReactive : ReactiveComponent
    {
        public LabelReactive(string name, string text)
        {
            Name = name;
            Text = text;
        }
    }
}