﻿using System;
using System.Windows.Forms;
using Bataillenavale.View;
using Bataillenavale.View.Router;

namespace Bataillenavale.Controller
{
    internal static class MainController
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            RenderHome();
            Application.Run(View.Main.Instance);
        }

        public static void RenderHome()
        {
            ViewRenderer.Render(Home.Instance);
        }
    }
}