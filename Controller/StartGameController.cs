﻿using Bataillenavale.Model;
using Bataillenavale.StoreManager.Actions;
using Bataillenavale.View.PositionBoats;
using Bataillenavale.View.Router;

namespace Bataillenavale.Controller
{
    public static class StartGameController
    {
        public static void StartLocalGame()
        {
            CreateLocalGame();
            RenderPositionBoats();
        }

        public static void CreateLocalGame()
        {
            var gameId = "LocalGame";
            Game.CreateGame(gameId);
            Player.CreatePlayer(gameId, PlayerType.Human);
            Player.CreatePlayer(gameId, PlayerType.Computer);
        }

        public static void RenderPositionBoats()
        {
            ViewRenderer.Render(PositionBoats.Instance);
        }
    }
}